const {src, dest, series, parallel} = require('gulp');

const browserSync = require('browser-sync').create();
const sass = require('gulp-sass');
const gulp = require("gulp");
// const path = require('path');

const postcss = require('gulp-postcss');


const uncss = require('postcss-uncss'),
del = require('del'),
// uglify = require('gulp-uglify'),
// concat = require('gulp-concat'),
cssnano = require('gulp-cssnano'),
rename = require('gulp-rename'),

autoprefixer = require('gulp-autoprefixer');





function emptyDist() {
    return del('./dist/**');
}


function copyHtml() {
    return src('./src/index.html')
        .pipe(dest('./dist'))
}

function copyJs() {
    return src('./src/js/index.js')
        .pipe(dest('./dist'))
}

function buildScss() {
    return src('./src/scss/**/*.scss')
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(postcss([
            uncss({html: ['./dist/index.html']})
        ]))
        .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true }))
        .pipe(cssnano())
        .pipe(rename({suffix: '.min'}))
        .pipe(dest('./src/css'));

}

function copyAssets() {
    return src('./src/assets/**')
        .pipe(dest('./dist/assets'));
}





function watchScss() {
    browserSync.init({
        server: "./src"
    });
    gulp.watch("./src/scss/*.scss");
    gulp.watch("src/*.html").on('change', browserSync.reload);
}


function compileSass () {
    return gulp.src("./src/scss/*.scss")
        .pipe(sass())
        .pipe(gulp.dest("./dist/css"))
        .pipe(browserSync.stream());
}


exports.html = copyHtml;
exports.scss = buildScss;
exports.assets = copyAssets;
exports.clear = emptyDist;
exports.js = copyJs;

exports.watch = watchScss;
exports.compile = compileSass;


exports.build = series(
    emptyDist,
    parallel(
        series(copyHtml, buildScss),
        copyAssets,
        // copyJs
    )
);


exports.dev = series(
    watchScss, compileSass
    );

exports.default = () => src(
    "style.scss"
).pipe(sass()).pipe(dest("./"));
